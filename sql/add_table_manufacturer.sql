CREATE TABLE `fleetcart`.`manufacturer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(191) NOT NULL,
  PRIMARY KEY (`id`))
COMMENT = 'nhà sản xuất : gồm tin tức, ecom, school, dating ...';

INSERT INTO `fleetcart`.`manufacturer` ( `name`) VALUES ( 'news');
INSERT INTO `fleetcart`.`manufacturer` ( `name`) VALUES ( 'school');
INSERT INTO `fleetcart`.`manufacturer` ( `name`) VALUES ( 'dating');
INSERT INTO `fleetcart`.`manufacturer` ( `name`) VALUES ( 'ecom');
INSERT INTO `fleetcart`.`manufacturer` ( `name`) VALUES ('real_estate');
